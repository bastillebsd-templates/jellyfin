## Status
[![pipeline status](https://gitlab.com/bastillebsd-templates/jellyfin/badges/main/pipeline.svg)](https://gitlab.com/bastillebsd-templates/jellyfin/commits/main)

## jellyfin
Bastille Template for Jellyfin

## Bootstrap
```shell
bastille bootstrap https://gitlab.com/bastillebsd-templates/jellyfin
```

## Usage
```shell
bastille template TARGET bastillebsd-templates/jellyfin
```
